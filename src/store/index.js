import Vue from 'vue';
import Vuex from 'vuex';

import {authStore} from "./modules/auth/auth";
import {notificationsStore} from "./modules/ui/notifications";
import {errorsStore} from "./modules/errors/erros";

Vue.use(Vuex);

export default new Vuex.Store({
  modules:{
    authStore,
    notificationsStore,
    errorsStore,
  },

})
