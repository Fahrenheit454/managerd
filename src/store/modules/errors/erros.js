
const state ={
  registerError:null
}
const actions ={
  setregistererror (context,payload){
    context.commit('SET_REGISTER_ERROR',payload)
  },
}
const mutations ={
  SET_REGISTER_ERROR(state,payload){
    state.registerError = payload
  }
}

const getters  ={
  registerErrorGetters : state => state.registerError
}

export const errorsStore ={
  state,
  actions,
  mutations,
  getters,

}
