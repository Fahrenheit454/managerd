import firebase from "firebase";
const state ={
  open : false
}
const actions ={
  SignUp (context,payload,) {
    context.commit('SET_LOADER',true)
    context.commit('SET_REGISTER_ERROR',null)
    firebase.auth().createUserWithEmailAndPassword(payload.email,payload.password).then(()=>{
      context.commit('SET_LOADER',false)
      console.log('The user has been created');
    }).catch(function(error){
      context.commit('SET_LOADER',false)
      context.commit('SET_REGISTER_ERROR',null)
      context.dispatch('setregistererror',{isError:true, errorMessage: error.message})
    })

  }
}
const mutations ={

}

const getters  ={
  userProblem : state => state.open
}

export const authStore ={
  state,
  actions,
  mutations,
  getters,

}
