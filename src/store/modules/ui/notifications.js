const state ={
  isLoading:false,

}
const actions ={
  setloader (context,payload) {
    context.commit('SET_LOADER',payload)
  },
}
const mutations ={

  SET_LOADER(state,payload){
    console.log("payload => ",payload)
    state.isLoading = payload
  },

}

const getters  ={
  isLoadingGetters : state => state.isLoading,
}

export const notificationsStore ={
  state,
  actions,
  mutations,
  getters,

}
