import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import firebase from 'firebase';

Vue.config.productionTip = false;

const  firebaseConfig = {
  apiKey: "AIzaSyCbuaMuT9x_U_koYS4rEro8mBNLW9pBMhU",
  authDomain: "rhmanager-195fb.firebaseapp.com",
  databaseURL: "https://rhmanager-195fb.firebaseio.com",
  projectId: "rhmanager-195fb",
  storageBucket: "rhmanager-195fb.appspot.com",
  messagingSenderId: "290950477450",
  appId: "1:290950477450:web:9124548e196c3f25"
};

firebase.initializeApp(firebaseConfig);
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app');
